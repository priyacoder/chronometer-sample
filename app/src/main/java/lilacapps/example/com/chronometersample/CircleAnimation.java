package lilacapps.example.com.chronometersample;

import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by 1021422 on 3/3/2018.
 */

public class CircleAnimation extends Animation {
    private CircleCustomView circle;

    private float initialAngle;
    private float offsetAngle;
    private float setToAngle;


    public CircleAnimation(CircleCustomView circleCustomView, float newAngle){
        this.initialAngle = circleCustomView.getAngle();
        this.offsetAngle = newAngle;
        this.circle = circleCustomView;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation){

        setToAngle = initialAngle + ((offsetAngle - initialAngle) * interpolatedTime);
        circle.setAngle(setToAngle);
        circle.requestLayout();
    }

    public float getSetAngle(){
        return setToAngle;
    }

}
