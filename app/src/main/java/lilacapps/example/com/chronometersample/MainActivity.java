package lilacapps.example.com.chronometersample;

import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    long timeWhenStopped = 0;
    private String TAG = getClass().getName();
    Chronometer simpleChronometer = null;
    CircleCustomView circleView;
    Button startButton, stopButton, resumeButton;
    private boolean debug_flag = true;
    private static final long PREFERRED_START_TIME_IN_MINS = 1;
    private static final long PREFERRED_START_TIME = PREFERRED_START_TIME_IN_MINS * 60 * 1000;
    private static final int arcAngle = 360;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the display values and set the dimensions to chronometer
        simpleChronometer = findViewById(R.id.chronometer);
        circleView = findViewById(R.id.circleView);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int x = displayMetrics.widthPixels;
        int y = displayMetrics.heightPixels;
        Log.d("Screen width", String.valueOf(x));
        Log.d("Screen height",String.valueOf(y));

        int length;
        if(x>y){
            length = (y/2) - 10;
        }else {
            length = (x/2) - 10;
        }
        simpleChronometer.getLayoutParams().width = simpleChronometer.getLayoutParams().height = length;
//        circleView.getLayoutParams().width = circleView.getLayoutParams().height = length * 2;
        // Set chronometer to count downwards
        simpleChronometer.setCountDown(true);
        simpleChronometer.setText(String.format(Locale.US,"%02d:00",PREFERRED_START_TIME_IN_MINS));
        simpleChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                if(chronometer.getText().toString().equalsIgnoreCase("00:00")){
                    chronometer.stop();
                }
            }
        });
        simpleChronometer.requestLayout();

        // Get the button control details
        startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(this);
        stopButton = findViewById(R.id.stopButton);
        stopButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        CircleAnimation animation = new CircleAnimation(circleView, arcAngle);;
        float currentAngle = 0f;

        int id = view.getId();
        switch(id){
            case R.id.startButton:
                String button_text = startButton.getText().toString();
                String start_timer = getResources().getString(R.string.start_timer);
                String pause_timer = getResources().getString(R.string.pause_timer);
                String resume_timer = getResources().getString(R.string.resume_timer);

                long resume_time = 0;
                if (debug_flag) Log.d(TAG,"Initial system time"+SystemClock.elapsedRealtime());
                // If user clicks on Start, it should continue counting the timer up and change the button to pause
                if(button_text.equals(start_timer)) {
                    if (debug_flag) Log.d(TAG,"timer initial base"+simpleChronometer.getBase());
                    startButton.setText(R.string.pause_timer);
                    // Set the preferred count downtime as base so that timer starts countdown from then
                    simpleChronometer.setBase(SystemClock.elapsedRealtime() + PREFERRED_START_TIME);
                    simpleChronometer.start();

                    // Animation for the timer
                    animation.setDuration(PREFERRED_START_TIME);
                    circleView.startAnimation(animation);
                    animation.setFillAfter(true);
                }
                //If user clicks on Pause, resume option should be shown now and the time when timer is stopped should be saved
                else if(button_text.equals(pause_timer)){
                    simpleChronometer.stop();
                    if (debug_flag) Log.d(TAG,"Elapsed time during pause"+SystemClock.elapsedRealtime());
                    timeWhenStopped = simpleChronometer.getBase() - SystemClock.elapsedRealtime();
                    startButton.setText(R.string.resume_timer);
                    if (debug_flag) Log.d(TAG,"Timer Stopped at "+timeWhenStopped);
                }
                // If user clicks on Resume, the timer should start from last time saved
                else if(button_text.equals(resume_timer)){
                    if (debug_flag) Log.d(TAG,"Elapsed time during resume"+SystemClock.elapsedRealtime());
                    resume_time = SystemClock.elapsedRealtime() + timeWhenStopped;
                    if (debug_flag) Log.d(TAG,"Timer to be start from "+resume_time);
                    simpleChronometer.setBase(resume_time);
                    simpleChronometer.start();
                    startButton.setText(R.string.pause_timer);

                    // Resume animation
//                    animation = new CircleAnimation(circleView,arcAngle - currentAngle);
//                    animation.setDuration(resume_time);
//
//                    circleView.startAnimation(animation);
//                    View parent = (View) circleView.getParent();
//                    parent.invalidate();

                }

                break;
                // If user clicks on Stop, the timer is stopped and reset.
            case R.id.stopButton:
                    if (debug_flag) Log.d(TAG,"Elapsed time during stop "+SystemClock.elapsedRealtime());
                    simpleChronometer.setBase(SystemClock.elapsedRealtime());
                    simpleChronometer.stop();
                    timeWhenStopped = 0;
                    startButton.setText(R.string.start_timer);
                    circleView.clearAnimation();
                break;
        }
    }
}
