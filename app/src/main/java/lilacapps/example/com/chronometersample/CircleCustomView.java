package lilacapps.example.com.chronometersample;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

/**
 * Created by 1021422 on 3/3/2018.
 */

public class CircleCustomView extends View {

    private static final int START_ANGLE_POINT = -90;
    private final Paint paint;
    private final RectF rectF;
    private float angle;

    public CircleCustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        final int strokeWidth = 40;
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(Color.RED);

        rectF = new RectF(40, 40, 230, 240);
        angle = 0;
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);

        int height = canvas.getHeight() / 2;
        int width = canvas.getWidth() / 2;
        rectF.set(width - 300, height - 300, width + 300, height + 300);
        canvas.drawArc(rectF,START_ANGLE_POINT, angle, false, paint);
    }

    public float getAngle(){
        return angle;
    }
    public void setAngle(float angle){
        this.angle = angle;
    }

}
